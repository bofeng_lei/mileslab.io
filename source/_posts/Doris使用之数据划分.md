---
title: Doris使用之数据划分
date: 2023-04-16
tags: [Apache Doris,MPP,大数据]
categories: Apache Doris
toc: true
---

内容：数据划分

<!-- more -->

# 基本概念

Table的划分

- 逻辑层面
  - **Row**，表中的一行数据
  - **Colum**，一行数据中不同含义的列值
    - **key**，维度列，聚合时，会按一列或几列组成的key进行聚合
    - **value**，指标列，聚合时，会被聚合

- 存储层面
  - **Partition**，Partition 可以视为是逻辑上最小的管理单元
  - **Tablet** ，Tablet 是数据移动、复制等操作的最小物理存储单元

> 在 Doris 的存储引擎中，用户数据被水平划分为若干个数据分片（Tablet，也称作数据分桶）。每个 Tablet 包含若干数据行。各个 Tablet 之间的数据没有交集，并且在物理上是独立存储的。
>
> 多个 Tablet 在逻辑上归属于不同的分区（Partition）。一个 Tablet 只属于一个 Partition。而一个 Partition 包含若干个 Tablet。因为 Tablet 在物理上是独立存储的，所以可以视为 Partition 在物理上也是独立。
>
> 若干个 Partition 组成一个 Table，数据的导入与删除，仅能针对一个 Partition 进行。

# 分区和分桶

> Doris 支持两层的数据划分。第一层是 Partition，支持 Range 和 List 的划分方式。第二层是 Bucket（Tablet），仅支持 Hash 的划分方式。
>
> 也可以仅使用一层分区。使用一层分区时，只支持 Bucket 划分。

## 分区

> Partition 列可以指定一列或多列，分区列必须为 **KEY 列**。
>
> 不论分区列是什么类型，在写分区值时，都需要加**双引号**。
>
> 分区数量理论上**没有上限**。
>
> 当不使用 Partition 建表时，系统会**自动生成**一个和表名同名的，全值范围的 Partition。该 Partition 对用户不可见，并且不可删改。
>
> 创建分区时**不可添加范围重叠**的分区。
>
> 分区的删除**不会**改变已存在分区的范围。
>
> 删除分区可能出现**空洞**

### Range

**示例**

```sql
-- Range Partition

CREATE TABLE IF NOT EXISTS example_db.example_range_tbl
(
    `user_id` LARGEINT NOT NULL COMMENT "用户id",
    `date` DATE NOT NULL COMMENT "数据灌入日期时间",
    `timestamp` DATETIME NOT NULL COMMENT "数据灌入的时间戳",
    `city` VARCHAR(20) COMMENT "用户所在城市",
    `age` SMALLINT COMMENT "用户年龄",
    `sex` TINYINT COMMENT "用户性别",
    `last_visit_date` DATETIME REPLACE DEFAULT "1970-01-01 00:00:00" COMMENT "用户最后一次访问时间",
    `cost` BIGINT SUM DEFAULT "0" COMMENT "用户总消费",
    `max_dwell_time` INT MAX DEFAULT "0" COMMENT "用户最大停留时间",
    `min_dwell_time` INT MIN DEFAULT "99999" COMMENT "用户最小停留时间"
)
ENGINE=OLAP
AGGREGATE KEY(`user_id`, `date`, `timestamp`, `city`, `age`, `sex`)
PARTITION BY RANGE(`date`)
(
    PARTITION `p201701` VALUES LESS THAN ("2017-02-01"),
    PARTITION `p201702` VALUES LESS THAN ("2017-03-01"),
    PARTITION `p201703` VALUES LESS THAN ("2017-04-01")
)
DISTRIBUTED BY HASH(`user_id`) BUCKETS 16
PROPERTIES
(
    "replication_num" = "3",
    "storage_medium" = "SSD",
    "storage_cooldown_time" = "2018-01-01 12:00:00"
);
```

- range分区列通常为时间列，以方便的管理新旧数据。

- 支持通过 `VALUES LESS THAN (...)` 仅指定上界，系统会将前一个分区的上界作为该分区的下界，生成一个左闭右开的区间。也支持通过 `VALUES [...)` 指定上下界，生成一个左闭右开的区间。

- 支持通过`FROM(...) TO (...) INTERVAL ...` 来批量创建分区(*since 1.2.0*)

  ```s'q'l
  FROM ("2022-01-03") TO ("2022-01-06") INTERVAL 1 DAY
  ```

- Range分区除了上述我们看到的单列分区，也支持**多列分区**，示例如下：

  ```sql
  PARTITION BY RANGE(`date`, `id`)
  (
      PARTITION `p201701_1000` VALUES LESS THAN ("2017-02-01", "1000"),
      PARTITION `p201702_2000` VALUES LESS THAN ("2017-03-01", "2000"),
      PARTITION `p201703_all`  VALUES LESS THAN ("2017-04-01")
  )
  ```

### List

示例

```sql
-- List Partition

CREATE TABLE IF NOT EXISTS example_db.example_list_tbl
(
    `user_id` LARGEINT NOT NULL COMMENT "用户id",
    `date` DATE NOT NULL COMMENT "数据灌入日期时间",
    `timestamp` DATETIME NOT NULL COMMENT "数据灌入的时间戳",
    `city` VARCHAR(20) NOT NULL COMMENT "用户所在城市",
    `age` SMALLINT COMMENT "用户年龄",
    `sex` TINYINT COMMENT "用户性别",
    `last_visit_date` DATETIME REPLACE DEFAULT "1970-01-01 00:00:00" COMMENT "用户最后一次访问时间",
    `cost` BIGINT SUM DEFAULT "0" COMMENT "用户总消费",
    `max_dwell_time` INT MAX DEFAULT "0" COMMENT "用户最大停留时间",
    `min_dwell_time` INT MIN DEFAULT "99999" COMMENT "用户最小停留时间"
)
ENGINE=olap
AGGREGATE KEY(`user_id`, `date`, `timestamp`, `city`, `age`, `sex`)
PARTITION BY LIST(`city`)
(
    PARTITION `p_cn` VALUES IN ("Beijing", "Shanghai", "Hong Kong"),
    PARTITION `p_usa` VALUES IN ("New York", "San Francisco"),
    PARTITION `p_jp` VALUES IN ("Tokyo")
)
DISTRIBUTED BY HASH(`user_id`) BUCKETS 16
PROPERTIES
(
    "replication_num" = "3",
    "storage_medium" = "SSD",
    "storage_cooldown_time" = "2018-01-01 12:00:00"
);
```

- 分区列支持 `BOOLEAN, TINYINT, SMALLINT, INT, BIGINT, LARGEINT, DATE, DATETIME, CHAR, VARCHAR` 数据类型，分区值为枚举值。只有当数据为目标分区枚

  举值其中之一时，才可以命中分区。

- Partition 支持通过 `VALUES IN (...)` 来指定每个分区包含的枚举值。

- List分区也支持**多列分区**，示例如下：

  ```sql
  PARTITION BY LIST(`id`, `city`)
  (
      PARTITION `p1_city` VALUES IN (("1", "Beijing"), ("1", "Shanghai")),
      PARTITION `p2_city` VALUES IN (("2", "Beijing"), ("2", "Shanghai")),
      PARTITION `p3_city` VALUES IN (("3", "Beijing"), ("3", "Shanghai"))
  )
  ```

## 分桶