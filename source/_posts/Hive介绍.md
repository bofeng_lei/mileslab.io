---
title: Hive介绍
date: 2016-04-23
tags: [Hive,大数据]
categories: Hadoop生态圈
toc: true
---

内容：Hive介绍

<!-- more -->

## Hive介绍

### 数据仓库

数据仓库与数据库

数据库 Database (Oracle, Mysql, PostgreSQL)主要用于**事务处理**，数据仓库 Datawarehouse (Amazon Redshift, Hive)主要用于**数据分析**。

用途上的不同决定了这两种架构的特点不同。

数据库(Database)的特点是：

- 相对复杂的表格结构，存储结构相对紧致，少冗余数据。
- 读和写都有优化。
- 相对简单的read/write query，单次作用于相对的少量数据。

数据仓库(Datawarehouse)的特点是：

- 相对简单的(Denormalized)表格结构，存储结构相对松散，多冗余数据。
- 一般只是读优化。
- 相对复杂的read query，单次作用于相对大量的数据（历史数据）。

### Hive产生背景 

直接使用MapReduce处理大数据，将面临以下问题：

```
MapReduce 开发难度大，学习成本高
Hdfs文件没有字段名、没有数据类型，不方便进行数据的有效管理 
使用MapReduce框架开发，项目周期长，成本高
```

Hive是基于Hadoop的一个**数据仓库工具**，可以将结构化的数据文件映射为一张表 (类似于RDBMS中的表)，并提供类SQL查询功能；Hive是由Facebook开源，用于解决海量结构化日志的数据统计。

### Hive和RDBMS对比 

```
-- 查询语言相似
	HQL <=> SQL 高度相似 由于SQL被广泛的应用在数据仓库中，因此，专门针对Hive的特性设计了类SQL的查询语言HQL。熟悉SQL开发的开发者可以很方便的使用Hive进行开发。
-- 数据规模
	Hive存储海量数据；RDBMS只能处理有限的数据集； 由于Hive建立在集群上并可以利用MapReduce进行并行计算，因此可以支持很大规模 的数据；而RDBMS可以支持的数据规模较小。
-- 执行引擎
	Hive的引擎是MR/Tez/Spark/Flink；RDBMS使用自己的执行引擎 Hive中大多数查询的执行是通过Hadoop提供的 MapReduce 来实现的。而RDBMS 通常有自己的执行引擎。
-- 数据存储
	Hive保存在HDFS上；RDBMS保存在本地文件系统 或 裸设备 Hive 的数据都是存储在HDFS中的。而RDBMS是将数据保存在本地文件系统或裸设备中。
-- 执行速度
	Hive相对慢（MR/数据量）；RDBMS相对快； Hive存储的数据量大，在查询数据的时候，通常没有索引，需要扫描整个表；加之 Hive使用MapReduce作为执行引擎，这些因素都会导致较高的延迟。而RDBMS对数据 的访问通常是基于索引的，执行延迟较低。当然这个低是有条件的，即数据规模较小， 当数据规模大到超过数据库的处理能力的时候，Hive的并行计算显然能体现出并行的优势。
-- 可扩展性
	Hive支持水平扩展；通常RDBMS支持垂直扩展，对水平扩展不友好 Hive建立在Hadoop之上，其可扩展性与Hadoop的可扩展性是一致的（Hadoop集群规模可以轻松超过1000个节点）。而RDBMS由于 ACID 语义的严格限制，扩展行非常有限。目前先进的并行数据库 Oracle 在理论上的扩展能力也只有100台左右。
-- 数据更新
	Hive对数据更新不友好；RDBMS支持频繁、快速数据更新 Hive是针对数据仓库应用设计的，数据仓库的内容是读多写少的。因此，Hive中不建 议对数据的改写，所有的数据都是在加载的时候确定好的。而RDBMS中的数据需要频繁、快速的进行更新。
```

### Hive的优缺点 

#### Hive的优点

- 学习成本低。Hive提供了类似SQL的查询语言，开发人员能快速上手；

- 处理海量数据。底层执行的是MapReduce 任务；

- 系统可以水平扩展。底层基于Hadoop； 功能可以扩展。Hive允许用户自定义函数；

- 良好的容错性。某个节点发生故障，HQL仍然可以正常完成；

- 统一的元数据管理。元数据包括：有哪些表、表有什么字段、字段是什么类型

#### Hive的缺点

- HQL表达能力有限；

- 迭代计算无法表达；

- Hive的执行效率不高(基于MR的执行引擎)；

- Hive自动生成的MapReduce作业，某些情况下不够智能；
- Hive的调优困难；

###  Hive架构

![](Hive介绍/Hive架构.png)

- 用户接口 CLI(Common Line Interface)：

  Hive的命令行，用于接收HQL，并返 回结果； JDBC/ODBC：是指Hive的java实现，与传统数据库JDBC类似； 

- WebUI：是指可通过浏览器访问Hive；

- Thrift Server 

  Hive可选组件，是一个软件框架服务，允许客户端使用包括Java、C++、Ruby和 其他很多种语言，通过 编程的

  方式远程访问Hive； 

- 元数据管理(MetaStore) 

  Hive将元数据存储在关系数据库中(如mysql、 derby)。Hive的元数据包括：数据库名、表名及类型、字段名称

  及数据类型、数 据所在位置等；

- 驱动程序(Driver) 

  - 解析器 (SQLParser) ：使用第三方工具（antlr）将HQL字符串转换成抽象语 法树（AST）；对AST进行语法

    分析，比如字段是否存在、SQL语义是否有 误、表是否存在； 

  - 编译器 (Compiler) ：将抽象语法树编译生成逻辑执行计划； 

  - 优化器 (Optimizer) ：对逻辑执行计划进行优化，减少不必要的列、使用分 区等； 

  - 执行器 (Executr) ：把逻辑执行计划转换成可以运行的物理计划；

### 数据类型

#### 基本数据类型

| 大类                           | 类型                                                         |
| ------------------------------ | ------------------------------------------------------------ |
| Integers(整型)                 | TINYINT -- 1字节的有符号整数 <br />SMALLINT -- 2字节的有符号整数 <br />INT -- 4字节的有符号整数 <br />BIGINT -- 8字节的有符号整数 |
| Floating point numbers(浮点数) | FLOAT -- 单精度浮点数 <br />DOUBLE -- 双精度浮点数           |
| Fixed point numbers(定点数)    | DECIMAL -- 17字节，任意精度数字。通常用户自定 义decimal(12, 6) |
| String(字符串)                 | STRING -- 可指定字符集的不定长字符串 <br />VARCHAR -- 1-65535长度的不定长字符串 <br />CHAR -- 1-255定长字符串 |
| Datetime(时间日期类 型)        | TIMESTAMP -- 时间戳（纳秒精度） <br />DATE -- 时间日期类型   |
| Boolean(布尔类型)              | TRUE / FALSE                                                 |
| Binary types(二进制类 型)      | BINARY -- 字节序列                                           |

##### 数据类型的隐式转换 

Hive的数据类型是可以进行隐式转换的，类似于Java的类型转换。如用户在查询中将 一种浮点类型和另一种浮点类型的值做对比，Hive会将类型转换成两个浮点类型中值 较大的那个类型，即：将FLOAT类型转换成DOUBLE类型；当然如果需要的话，任意 整型会转化成DOUBLE类型。 Hive 中基本数据类型遵循以下层次结构，按照这个层 次结构，子类型到祖先类型允许隐式转换。

**总的来说数据转换遵循以下规律：**

```shell
hive> select '1.0'+2; 
OK 3.0 
hive> select '1111' > 10; 
hive> select 1 > 0.8;
```

##### 数据类型的显示转换 

使用cast函数进行强制类型转换；如果强制类型转换失败，返回NULL

```shell
hive> select cast('1111s' as int); 
OK NULL 
hive> select cast('1111' as int); 
OK 1111
```

#### 集合数据类型

Hive支持集合数据类型，包括array、map、struct、union

| 类型   | 描述                                           | 字面量示例                                                   |
| ------ | ---------------------------------------------- | ------------------------------------------------------------ |
| ARRAY  | 有序的相同数据类型的集合                       | array(1,2)                                                   |
| MAP    | key-value对。key必须是基 本数据类型，value不限 | map('a', 1, 'b',2)                                           |
| STRUCT | 不同类型字段的集合。类似 于C语言的结构体       | struct('1',1,1.0), named_struct('col1', '1', 'col2', 1, 'clo3', 1.0) |
| UNION  | 不同类型的元素存储在同一 字段的不同行中        | create_union(1, 'a', 63)                                     |

和基本数据类型一样，这些类型的名称同样是保留字；

ARRAY 和 MAP 与 Java 中的 Array 和 Map 类似；

STRUCT 与 C 语言中的 Struct 类似，它封装了一个命名字段集合，复杂数据类型允 许任意层次的嵌套；

```shell
-- array
	-- 查看array
	hive> select array(1,2,3);
	[1,2,3]
	-- 查看array元素，使用[]
	hive> select arr[1] from (select array(1,2,3) arr) tmp;
	2
-- map
	-- 查看map
	hive> select map('a',1,'b',2);
	{"a":1,"b":2}
	-- 查看map元素，使用[],key不存在时返回NULL
	hive> select m["a"] from (select map('a',1,'b',2) m) tmp;
	1
	hive> select m["c"] from (select map('a',1,'b',2) m) tmp;
	NULL
-- struct
	-- 查看struct
	hive>  select struct('username1', 7, 1288.68);
	{"col1":"username1","col2":7,"col3":1288.68}
	-- 命名struct字段
	hive> select named_struct("name", "username1", "id", 7,"salary", 12880.68);
	{"name":"username1","id":7,"salary":12880.68}
	-- 使用 列名.字段名 访问具体信息
	hive> select userinfo.id from (select named_struct("name", "username1", "id",
		  7, "salary", 12880.68) userinfo) tmp;
	id
	7
-- union
	-- 查看union
	hive> select create_union(0, "zhansan", 19, 8000.88) info;
	info
	{0:"zhansan"}
```
