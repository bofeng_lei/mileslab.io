---
title: Doris介绍
date: 2023-03-14
tags: [Apache Doris,MPP,大数据]
categories: Apache Doris
toc: true
---

内容：Doris介绍、Doris原理

<!-- more -->

# Doris架构

## 整体架构

Doris整体架构如下图所示，Doris 架构非常简单，只有两类进程

- **Frontend（FE）**，主要负责用户请求的接入、查询解析规划、元数据的管理、节点管理相关工作。
- **Backend（BE）**，主要负责数据存储、查询计划的执行

![整体架构](Doris介绍/Doris整体架构i.png)

## 接口协议

Doris 采用 MySQL 协议，高度兼容 MySQL 语法，支持标准 SQL，用户可以通过各类客户端工具来访问 Doris，并支持与 BI 工具的无缝对接。

## 存储引擎

Doris 采用列式存储，按列进行数据的编码压缩和读取，能够实现极高的压缩比，同时减少大量非相关数据的扫描，从而更加有效利用 IO 和 CPU 资源。

## 存储模型

Doris 支持多种存储模型，针对不同的场景做了针对性的优化：

- Aggregate Key 模型：相同 Key 的 Value 列合并，通过提前聚合大幅提升性能
- Unique Key 模型：Key 唯一，相同 Key 的数据覆盖，实现行级别数据更新
- Duplicate Key 模型：明细数据模型，满足事实表的明细存储			

## 查询引擎

Doris 采用 MPP 的模型，节点间和节点内都并行执行，也支持多个大表的分布式 Shuffle Join，从而能够更好应对复杂查询。

![查询引擎架构](Doris介绍/Doris查询引擎架构i.png)

# 使用场景

- 报表分析
- 即席查询
- 统一数仓构建
- 数据湖联邦查询

**引用**：https://doris.apache.org/zh-CN/docs/dev/summary/basic-summary