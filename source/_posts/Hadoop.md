---
title: Hadoop入门
date: 2016-04-05
categories: [Hadoop,大数据]
tags: Hadoop
toc: true
---
内容：Hadoop介绍、Hadoop集群搭建

<!--more -->

# 大数据特点

![](Hadoop/5V.png)

```
-Volume
   大量  
   采集、存储和计算的数据量都非常大。
-Velocity
   高速  
   在大数据时代，数据的创建、存储、分析都要求被高速处理。
   比如电商网站的个性化推荐尽可能要求实时完成推荐，这也是大数据区别于传统数据挖掘的显著特征。
-Variety
   多样 
   数据形式和来源多样化。包括结构化、半结构化和非结构化数据。
   具体表现为网络日志、音 频、视频、图片、地理位置信息等等，多类型的数据对数据的处理能力提出了更高的要求 
-Veracity
   真实  
   确保数据的真实性，才能保证数据分析的正确性。
-Value
   低价值  
   数据价值密度相对较低，或者说是浪里淘沙却又弥足珍贵。
   互联网发展催生了大量数据，信 息海量，但价值密度较低，如何结合业务逻辑并通过强大的机器算法来挖掘数据价值
   是大数据时代需要解决的问题，也是一个有难度的课题。 
```

#  Hadoop简介 

##  什么是Hadoop 

狭义上说Hadoop就是一个框架平台，由HDFS、MapReduce、YARN组成

广义上讲Hadoop代表大数据的一个技术生态 圈，包括很多其他软件框架

> Hadoop生态圈技术栈
>
> Hadoop（HDFS + MapReduce + Yarn）
> Hive 数据仓库工具
> HBase 海量列式非关系型数据库
> Flume 数据采集工具
> Sqoop ETL工具
> Kafka 高吞吐消息中间件
> ......

##  Hadoop的起源

Hadoop 的发展历程可以用如下过程概述：

![](Hadoop/hadoop起源.png)



- Hadoop早起源于Nutch，Nutch 的创始人是Doug Cutting。 Nutch 是一个开源 Java 实现的搜索引擎。它提供了我们运行自己的搜索引擎所需的全部工具。包括全文搜索和Web爬虫，但随着抓取网页数量的增加，遇到了严重的可扩展性问题——如何解决 数十亿网页的存储和索引问题 。

- 2003年、2004年谷歌发表的两篇论文为该问题提供了可行的解决方案。

  GFS，可用于处理海量网页的存储；

  MapReduce，可用于处理海量网页的索引计算问题。

  - Google的三篇论文（三驾马车） 
    - ​	GFS：Google的分布式文件系统（Google File System）
    - ​	MapReduce：Google的分布式计算框架 
    - ​	BigTable：大型分布式数据库 

  - 发展演变关系： 
    - ​    GFS	—>	HDFS
    - ​    Google MapReduce   —>  Hadoop MapReduce
    - ​    BigTable  —>  HBase

- 随后，Google公布了部分GFS和MapReduce思想的细节，Doug Cutting等人用2年的业余时间实现了DFS和MapReduce机制，使Nutch性能飙升。 

- 2005年，Hadoop 作为Lucene的子项目Nutch的一部分引入Apache 

- 2006年，Hadoop从Nutch剥离出来独立 

- 2008年，Hadoop成为Apache的顶级项目 Hadoop这个名字来源于Hadoop之父Doug Cutting儿子的毛绒玩具象

<img src="Hadoop/hadoop图标.png" style="zoom:50%;" />

## Hadoop版本演变

<img src="Hadoop/hadoop版本.png" style="zoom:50%;" />

- 0.x 系列版本：Hadoop当中早的一个开源版本，在此基础上演变而来的1.x以及2.x的版本

- 1.x 版本系列：Hadoop版本当中的第二代开源版本，主要修复0.x版本的一些bug等 

- 2.x 版本系列：架构产生重大变化，引入了yarn平台等许多新特性

- 3.x 版本系列：EC技术（纠删码，Erasure Code）、YARN的时间轴服务等新特性

## Hadoop发行版本

企业中主要用到的三个版本分别是：

- Apache Hadoop版本（原始的，所有发行版均基于这个版 本进行改进）

  > 官网地址：http://hadoop.apache.org/ 
  > 优点：拥有全世界的开源贡献，代码更新版本比较快 
  > 缺点：版本的升级，版本的维护，以及版本之间的兼容性，学习非常方便 
  > Apache所有软件的下载地址（包括各种历史版本）：http://archive.apache.org/dist/ 

- Cloudera版本（Cloudera’s Distribution Including Apache Hadoop，简称“CDH”）

  > 官网地址：https://www.cloudera.com/
  > Cloudera主要是美国一家大数据公司在Apache开源Hadoop的版本上，通过自己公司内部的各种补丁，实现版本之间的稳定运行，大数据生态圈的各个版本的软件都提供了对应的版本，解决了版本的升级困难，版本兼容性等各种问题，生产环境强烈推荐使用

- Hortonworks版本（Hortonworks Data Platform，简称“HDP”）。

  > 官网地址：https://hortonworks.com/
  > hortonworks主要是雅虎主导Hadoop开发的副总裁，带领二十几个核心成员成立Hortonworks，核心产品软件HDP（ambari），HDF免费开源，并且提供一整套的web管理界面，供我们可以通过web界面管理我们的集群状态，web管理界面软件HDF网址（http://ambari.apache.org/） 

##  Hadoop的优缺点 

- 优点
  - 1、高可靠性

    Hadoop按位存储和处理数据的能力值得人们信赖。

  - 2、高扩展性

    Hadoop是在可用的计算机集簇间分配数据并完成计算任务的，这些集簇可以方便地扩展到数以千计的节点中。

  - 3、高效性

    Hadoop能够在节点之间动态地移动数据，并保证各个节点的动态平衡，因此处理速度非常快。

  - 4、高容错性

    Hadoop能够自动保存数据的多个副本，并且能够自动将失败的任务重新分配。Hadoop带有用Java语言编写的框架，因此运行在Linux生产平台上是非常理想的。Hadoop上的应用程序也可以使用其他语言编写，比如C++。

- 缺点

  - 1、延迟高

    Hadoop不适用于低延迟数据访问。

  - 2、小文件问题

    Hadoop不能高效存储大量小文件。

  - 3、修改

    Hadoop不支持多用户写入并任意修改文件。

## Hadoop的组成

HDFS、MapReduce、YARN、common

> 1、HDFS(Hadoop Distribute File System)，分布式文件系统 
>
> 2、MapReduce，分布式的离线并行计算框架
>
> 3、YARN，作业调度与集群资源管理的框架
>
> 4、common，工具模块（Configuration、RPC、序列化机制、日志操作）
